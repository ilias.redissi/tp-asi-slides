package services.impl;

import cpe.MessageReceiver;
import cpe.MessageSender;
import cpe.models.Answer;
import cpe.models.LoginPwd;
import cpe.models.Role;
import cpe.models.User;
import services.WatcherAuthRestService;

import javax.inject.Inject;


public class WatcherAuthRestServiceImpl implements WatcherAuthRestService {

    @Inject
    MessageSender sender;

    @Inject
    MessageReceiver receiver;

    @Override
    public Answer auth(LoginPwd lp) {
        Answer a;
        User u = new User("lastname"+lp.getLogin(), "surname"+lp.getLogin(), lp, Role.ADMIN);
        System.out.println(u.toString());

        sender.sendMessage(u);
        u = receiver.receiveMessage();

        a = new Answer(u);
        return a;
    }
    //{login : user.getLogin(), validAuth :true, role : ‘ADMIN’}
    //curl -d '{"login":"Kalimero", "pwd": "lel"}' -H "Content-Type: application/json" -X POST http://127.0.0.1:8080/FrontAuthWatcherWebService/rest/WatcherAuth/
}
