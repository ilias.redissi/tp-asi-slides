package services;

import cpe.models.Answer;
import cpe.models.LoginPwd;
import cpe.models.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/WatcherAuth")
public interface WatcherAuthRestService {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Answer auth(LoginPwd lp);
}
