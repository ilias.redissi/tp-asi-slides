package cpe;

import cpe.models.User;

import javax.ejb.Local;

@Local
public interface MessageSenderQueue {
    void sendMessage(String message);
    void sendMessage(User user);
}
