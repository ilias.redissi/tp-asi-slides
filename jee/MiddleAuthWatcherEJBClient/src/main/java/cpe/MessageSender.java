package cpe;

import cpe.models.User;

import javax.ejb.Local;

@Local
public interface MessageSender {
    void sendMessage(String message);
    void sendMessage(User user);
}
