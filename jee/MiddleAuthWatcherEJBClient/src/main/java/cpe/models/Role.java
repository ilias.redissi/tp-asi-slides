package cpe.models;

public enum Role {
    NONE,
    ADMIN,
    WATCHER
}
