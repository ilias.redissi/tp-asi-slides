package cpe.models;

public class Answer {
    public String login;
    public boolean validAuth;
    public Role role;

    public Answer(String login, boolean validAuth, Role role) {
        this.login = login;
        this.validAuth = validAuth;
        this.role = role;
    }

    public Answer(User u){
        this.login = "";
        this.validAuth = false;
        this.role = Role.NONE;

        if (u.getLogin() != null) {
            this.login = u.getLogin();
        }

        if (u.getRole() != null){
            this.validAuth = !(u.getRole().equals(Role.NONE));
            this.role = u.getRole();
        }
    }
}
