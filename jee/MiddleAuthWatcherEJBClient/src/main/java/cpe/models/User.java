package cpe.models;

import javax.persistence.*;
import java.io.Serializable;

//create table USERS (user_id INT unsigned auto_increment primary key, lastname varchar(30) not null, surname varchar(30) not null, login varchar(30) not null unique, pwd varchar(30) not null, role varchar(10) not null);
//insert into USERS (lastname, surname, login, pwd, role) values ('BONNARD', 'bg', 'Kalimero', 'lel', 'ADMIN');
@Entity
@Table(name = "USERS")
@NamedQueries({
    @NamedQuery(name = "Users.list", query = "select u from User u"),
    @NamedQuery(name = "Users.byLogin", query = "select u from User u where u.login = :login")
})
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Integer id;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "surName")
    private String surName;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "pwd")
    private String pwd;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    public User(Integer id, String lastName, String surName, String login, String pwd, Role role) {
        this.id = id;
        this.lastName = lastName;
        this.surName = surName;
        this.login = login;
        this.pwd = pwd;
        this.role = role;
    }

    public User(String lastName, String surName, String login, String pwd, Role role) {
        this.lastName = lastName;
        this.surName = surName;
        this.login = login;
        this.pwd = pwd;
        this.role = role;
    }

    public User(String lastName, String surName, LoginPwd lp, Role role) {
        this.lastName = lastName;
        this.surName = surName;
        this.login = lp.getLogin();
        this.pwd = lp.getPwd();
        this.role = role;
    }

    public User() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof User)) {
            return false;
        }

        User u = (User) o;

        return (u.login.equals(this.login) && u.pwd.equals(this.pwd));
    }

    @Override
    public String toString(){
        return "M./Mme " + this.lastName + " aka \"" + this.surName + "\" identified by " + this.login + ":" + this.pwd + " has role " + this.role;
    }
}

