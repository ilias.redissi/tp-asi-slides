package cpe.models;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class UserDao {
    @PersistenceContext
    EntityManager em;

    public List<User> listUser() {
        return em.createNamedQuery("Users.list").getResultList();
    }

    public User getUserByLogin(String login, String pwd) {
        User udb;
        User u = new User();
        u.setRole(Role.NONE);
        u.setLogin(login);
        try{
            udb = (User) em.createNamedQuery("Users.byLogin").setParameter("login", login).getSingleResult();
            if (udb.getPwd().equals(pwd)) {
                return udb;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return u;
    }
}
