package cpe;

import cpe.models.User;

import javax.ejb.Local;

@Local
public interface MessageReceiver {
    User receiveMessage();
}
