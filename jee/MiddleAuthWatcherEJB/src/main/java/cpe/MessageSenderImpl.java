package cpe;

import cpe.models.User;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@LocalBean
@Stateless
public class MessageSenderImpl implements MessageSender {
    @Inject
    JMSContext context;
    @Resource(mappedName = "java:/jms/watcherAuthJMS")
    Topic topic;

    @Override
    public void sendMessage(String message) {
        context.createProducer().send(topic, message);
    }

    @Override
    public void sendMessage(User user) {
        context.createProducer().send(topic, user);
    }
}
