package cpe;

import cpe.models.User;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@Stateless
@LocalBean
public class MessageReceiverImpl implements MessageReceiver {
    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/queue/watcherqueue")
    Queue queue;

    @Override
    public User receiveMessage() {
        User u = new User();
        Message message = context.createConsumer(queue).receive(1000);
        if (message instanceof ObjectMessage) {
            try {
                u = (User) (((ObjectMessage) message).getObject());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
        System.out.println("received good auth : " + u);
        return u;
    }
}
