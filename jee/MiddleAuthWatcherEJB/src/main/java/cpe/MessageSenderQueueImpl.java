package cpe;
import cpe.models.User;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
/**
 * Session Bean implementation class MessageSenderQueue
 */
@Stateless
public class MessageSenderQueueImpl implements MessageSenderQueue {
    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/queue/watcherqueue")
    Queue queue;

    public void sendMessage(String message) {
        context.createProducer().send(queue, message);
    }
    public void sendMessage(User user) {
        context.createProducer().send(queue, user);
    }
}