# TP ASI
De Kévin Bonnard, Yann Cayrac et Ilias Redissi

## Parties réalisés
### React
- Affichage des contenus
- Affichage de la présentation
- Modification d'un slide : titre, description et contenu via drag'n'drop
- Ajout/Suppression d'un slide
- Communication vers le serveur via des websockets

### Node.js
- Sauvegarde d'un contenu
- Affichage ou redirection vers un contenu
- Communication avec la page admin via des websockets
- Chargement et sauvegarde d'une présentation

### JEE
- Authentifaction via webservice
  - reception POST,
  - send login/pwd in topic
  - recieve with message-driven,
  - verification user avec JPA à travers un USER DAO
  - envoie utilisateur complet dans queue
  - reception et création de la réponse JSON avec le role et validAuth.
- Utile :
  - La base s'appelle "tp"
  - insert into USERS (lastname, surname, login, pwd, role) values ('', '', '', '', '');
  - utiliser les login/pwd "elprof" et "1234" pour s'authentifier en tant qu'ADMIN
  - utiliser les login/pwd "js" et "jspwd" pour s'authentifier en tant que WATCHER
  - tout autre tentative renverra le role NONE et une mauvaise authentification.

### Parties non-réalisés
### React
- Sauvegarde d'une présentation

### Node.js
- Récupération de la liste des contenus
- Affichage des pages admin et watch

### JEE

## Vidéo
Nous n'avons pas mis de vidéo car les différentes parties ne sont pas actuellement pas dépendantes l'une de l'autre. 

Toute les parties peuvent être testés indépendamment. 
La communication entre la page admin et le serveur node est fonctionnelle.

## Lien git
[Gitlab](https://gitlab.com/ilias.redissi/tp-asi-slides)

## Procédure pour désactiver l'auth
L'authentification n'étant pas relié au autres parties du tp, il n'y pas de procédure particulière pour la désactiver. 