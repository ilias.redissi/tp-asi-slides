import React, { Component } from 'react'
import './editMetaSlid.css'

export default class EditMetaSlid extends Component {

    handleChangeTitle = event => {
        if (this.props.onChangeTitle) {
            this.props.onChangeTitle(event.target.value)
        }
    }

    handleChangeTxt = event => {
        if (this.props.onChangeTxt) {
            this.props.onChangeTxt(event.target.value)
        }
    }

    render() {
        return (
            <div className="form-group">
                <label htmlFor="currentSlideTitle">Title </label> 
                <input type="text" className="form-control" id="currentSlideTitle" onChange={this.handleChangeTitle} value={this.props.title} />
                <label htmlFor="currentSlideText">Text</label> 
                <textarea rows="5" type="text" className="form-control" id="currentSlideText" onChange={this.handleChangeTxt} value={this.props.txt}> </textarea> 
            </div>
        );
    }
}
