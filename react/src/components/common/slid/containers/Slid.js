import React, { Component } from 'react'
import Content from '../../content/containers/Content';
import EditMetaSlid from '../components/EditMetaSlid';

import { connect } from 'react-redux';
import { setSelectedSlid, updateSlid } from '../../../../actions'

class Slid extends Component {

    updateSelectedSlid = () => {
        const tmpSlid = {
            id: this.props.id,
            title: this.props.title,
            txt: this.props.txt,
            content_id: this.props.content_id
        }
        this.props.dispatch(setSelectedSlid(tmpSlid))
    }

    displayContent = () => {
        let contentMap = this.props.contentMap;
        if (contentMap === undefined) {
            return (<div></div>)
        }
        let content_id = this.props.content_id;
        let content = contentMap[content_id];
        if (content) {
            return (
                <Content id={content.id} title={content.title} src={content.src} type={content.type} />
            )
        }
    }

    displayMode = () => {
        switch(this.props.displayMode) {
            case 'SHORT':
                return (
                    <div className="card pl-3 pr-3 mb-3">
                        <div>{this.props.title}</div>
                        <div>{this.props.txt}</div>
                        {this.displayContent()}
                    </div>
                )
            case 'FULL_MNG':
                return (
                    <div className="card pl-3 pr-3">
                        <div>{this.props.title}</div>
                        <div>{this.props.txt}</div>
                        {this.displayContent()}
                        <EditMetaSlid title={this.props.title} txt={this.props.txt} onChangeTitle={this.updateTitle} onChangeTxt={this.updateTxt} />
                    </div>
                )
            default:
                return (<div>Choose a display mode.</div>)
        }
    }

    updateTitle = (title) => {
        const tmpSlid = {
            id: this.props.id,
            content_id: this.props.content_id,
            title: title,
            txt: this.props.txt
        }

        this.props.dispatch(updateSlid(tmpSlid))

        this.props.dispatch(setSelectedSlid(tmpSlid))
    }

    updateTxt = (txt) => {
        const tmpSlid = {
            id: this.props.id,
            content_id: this.props.content_id,
            title: this.props.title,
            txt: txt
        }

        this.props.dispatch(updateSlid(tmpSlid))

        this.props.dispatch(setSelectedSlid(tmpSlid))
    }

    allowDrop = (event) => {
        event.preventDefault()
    }

    drop = (event) => {
        const tmpSlid = {
            id: this.props.id,
            content_id: event.dataTransfer.getData("content_id"),
            title: this.props.title,
            txt: this.props.txt
        }

        this.props.dispatch(updateSlid(tmpSlid))

        this.props.dispatch(setSelectedSlid(tmpSlid))
    }

    handleClick = () => {
        if (this.props.onClick) {
            console.log(this.onClick)
            this.props.onClick()
        }
    }

    render() {
        return (
            <div onDragOver={this.allowDrop} onDrop={this.drop} onClick={this.handleClick}>
                {this.displayMode()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        contentMap: state.updateModelReducer.contentMap,
    }
};

export default connect(mapStateToProps)(Slid);