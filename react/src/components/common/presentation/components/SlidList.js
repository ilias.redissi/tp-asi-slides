import React, { Component } from 'react'
import Slid from '../../slid/containers/Slid';

import { connect } from 'react-redux';
import { setSelectedSlid } from '../../../../actions'

class SlidList extends Component {

    updateSelectedSlid = slid => {
        this.props.dispatch(setSelectedSlid(slid))
    }

    displaySlids = () => {
        let slids = this.props.slidArray
        if (!slids) {
            return (<div></div>)
        }
        return slids.map(slid => {
            return (
                <Slid 
                    key={slid.id} 
                    content_id={slid.content_id} 
                    title={slid.title} 
                    txt={slid.txt} 
                    id={slid.id} 
                    displayMode="SHORT" 
                    onClick={() => this.updateSelectedSlid(slid)} />
            )
        });
    }

    render() {
        return (<div>{this.displaySlids()}</div>)
    }
}

export default connect()(SlidList)