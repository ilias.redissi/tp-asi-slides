import React, { Component } from 'react'

export default class EditMetaPres extends Component {

  handleChangeTitle = event => {

  }

  handleChangeDescription = event => {
    
  }

  render() {
    return (
        <div className="form-group">
            <label htmlFor="currentPresTitle">Title </label> 
            <input type="text" className="form-control" id="currentPresTitle" onChange={this.handleChangeTitle} value={this.props.title} />
            <label htmlFor="currentPresText">Text</label> 
            <textarea rows="5" type="text" className="form-control" id="currentPresDescription" onChange={this.handleChangeDescription} value={this.props.description}> </textarea> 
        </div>
    )
  }
}
