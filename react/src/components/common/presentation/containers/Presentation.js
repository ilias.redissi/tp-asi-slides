import React, { Component } from 'react'
import EditMetaPres from '../components/EditMetaPres';
import SlidList from '../components/SlidList';

import { connect } from 'react-redux';

class Presentation extends Component {
  render() {

    console.log(this.props.pres.title)
    return (
      <div className="vertical-scroll h-100">
        <EditMetaPres title={this.props.pres.title} description={this.props.pres.description} />
        <SlidList slidArray={this.props.pres.slidArray} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
      pres: state.updateModelReducer.presentation,
  }
};

export default connect(mapStateToProps)(Presentation)
