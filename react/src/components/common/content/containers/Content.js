import React, { Component } from 'react'
import { updateDraggedElt } from '../../../../actions'
import { connect } from 'react-redux';

class Content extends Component {


    processDisplayProperties = () => {
        if (this.props.onlyContent === false) {
            return (<div>Id: {this.props.id} Title: {this.props.title} Type: {this.props.type} Source: {this.props.src}</div>);
        }

        return;
    }

    processDisplay = () => {
        switch(this.props.type){
            case 'img':
            case 'img_url':
                return (
                    <img className='card-img-top' width="100%" src={this.props.src}></img>
                );
            default:
                return (
                    <object className="card-img-top" width="100%" height="100%" data={this.props.src}></object>
                )
        }
    }

    drag = (event) => {
        event.dataTransfer.setData("content_id", this.props.id)
    }

    dragged = () => {
        this.props.dispatch(updateDraggedElt(this.props.id))
    }

    render() {
        return (
        <div className="card mb-3" draggable="true" onDragStart={(event) => this.drag(event)} onDragEnd={this.dragged}>
            {this.processDisplay()}
            <div className="card-body">
                {this.processDisplayProperties()}
            </div>
        </div>
        )
    }
}

export default connect()(Content);