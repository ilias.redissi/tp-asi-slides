import React, { Component } from 'react'
import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core'
import './addContentPanel.css';

import { connect } from 'react-redux';
import { addContent } from '../../../actions'

class AddContentPanel extends Component {

    constructor(props) {
        super(props)

        this.state = {
            type: "image",
            title: '',
            src: ''
        }
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        })
    }

    handleClose = () => {
        this.props.onClose()
    }

    handleAdd = () => {
        this.props.dispatch(addContent(this.state))
        this.props.onClose()
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleClose}
                maxWidth="sm"
                aria-labelledby="form-dialog-title"
                fullWidth={true}
                >
                <DialogTitle id="form-dialog-title">Add a new content</DialogTitle>
                <DialogContent>
                    <form className="form-group">
                        <label htmlFor="title">Title</label>
                        <input className="form-control" id="title" onChange={this.handleChange('title')} value={this.state.title}/>
                        <label htmlFor="type">Content type</label>
                        <select className="form-control" id="type" onChange={this.handleChange('type')} value={this.state.type}>
                        <option value="image">Image</option>
                        <option value="image_url">Image URL</option>
                        <option value="video">Video</option>
                        <option value="web">Web</option>
                        </select>
                        <label htmlFor="url">URL</label>
                        <input className="form-control" id="url" onChange={this.handleChange('src')} value={this.state.src}/>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                    Cancel
                    </Button>
                    <Button onClick={this.handleAdd} color="primary">
                    Add
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default connect()(AddContentPanel);