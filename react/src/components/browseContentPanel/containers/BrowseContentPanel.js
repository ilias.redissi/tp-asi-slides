import React, { Component } from 'react'
import { Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import './browseContentPanel.css';
import Content from '../../common/content/containers/Content';

import { connect } from 'react-redux';
import AddContentPanel from '../components/AddContentPanel';

class BrowseContentPanel extends Component {

    constructor(props) {
        super(props)

        this.state = {
            open: false
        }
    }

    displayContent = () => {
        let contents = this.props.contentMap
        if (contents === undefined) {
            return (<div></div>)
        }
        return Object.keys(contents).map((contentKey, index) => {
            let content = contents[contentKey];
            return (
                <Content src={content.src} title={content.title} type={content.type} id={content.id} key={content.id}></Content>
            )
        });
    }

    handleFab = () => {
        this.setState({
            open: true
        })
    }

    handleCloseDialog = () => {
        this.setState({
            open: false
        })
    }

    render() {
        let display_result = this.displayContent();
        return(
            <div className="h-100 vertical-scroll">
                {display_result}
                <Button onClick={this.handleFab} variant="fab" color="primary" aria-label="Add" className="fab">
                    <AddIcon />
                </Button>
                <AddContentPanel open={this.state.open} onClose={this.handleCloseDialog} />
            </div>
            
        );
    }
}

const mapStateToProps = (state) => {
    return {
        contentMap: state.updateModelReducer.contentMap,
    }
};


export default connect(mapStateToProps)(BrowseContentPanel)