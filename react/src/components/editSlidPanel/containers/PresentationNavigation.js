import React, { Component } from 'react'

import { connect } from 'react-redux';
import { sendNavCmd } from '../../../actions'

class PresentationNavigation extends Component {

    handleClick = (type) => {
        this.props.dispatch(sendNavCmd(type))
    }

    render() {
        return (
        <div className="btn-group d-flex justify-content-center pb-3 pt-3" role="group" aria-label="Basic example">
            <button onClick={() => this.handleClick("FIRST_CMD")} type="button" className="btn btn-secondary" aria-label="First">
                <i className="fas fa-step-backward" aria-hidden="true"></i>
            </button>
            <button onClick={() => this.handleClick("PREVIOUS_CMD")} type="button" className="btn btn-secondary" aria-label="Previous">
                <i className="fas fa-backward" aria-hidden="true"></i>
            </button>
            <button onClick={() => this.handleClick("PLAY_CMD")} type="button" className="btn btn-secondary" aria-label="Play">
                <i className="fas fa-play" aria-hidden="true"></i>
            </button>
            <button onClick={() => this.handleClick("PAUSE_CMD")} type="button" className="btn btn-secondary" aria-label="Pause">
                <i className="fas fa-pause" aria-hidden="true"></i>
            </button>
            <button onClick={() => this.handleClick("NEXT_CMD")} type="button" className="btn btn-secondary" aria-label="Next">
                <i className="fas fa-forward" aria-hidden="true"></i>
            </button>
            <button onClick={() => this.handleClick("LAST_CMD")} type="button" className="btn btn-secondary" aria-label="Last">
                <i className="fas fa-step-forward" aria-hidden="true"></i>
            </button>
        </div>
        )
    }
}

export default connect()(PresentationNavigation)