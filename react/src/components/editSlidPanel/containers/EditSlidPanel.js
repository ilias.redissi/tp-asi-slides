import React, { Component } from 'react'
import Slid from '../../common/slid/containers/Slid';
import './editSlidPanel.css';

import { connect } from 'react-redux';
import { updateSlid } from '../../../actions'

class EditSlidPanel extends Component {

    updateCurrentSlid = (id, title, txt, content_id) => {
        this.props.dispatch(updateSlid({
            id: id,
            content_id: content_id,
            title: title,
            txt: txt
        }))
    }
    render() {
        if (this.props.selected_slid.id === undefined) {
            return <div></div>;
        }

        return (
            <div className="h-100 vertical-scroll">
                <Slid 
                    id={this.props.selected_slid.id} 
                    content_id={this.props.selected_slid.content_id} 
                    title={this.props.selected_slid.title} 
                    txt={this.props.selected_slid.txt}
                    displayMode="FULL_MNG" />
            </div>
        )
    }
}

const mapStateToProps = (state) => { 
    return {
        selected_slid: state.selectedReducer.slid,
    }
};

export default connect(mapStateToProps)(EditSlidPanel);

