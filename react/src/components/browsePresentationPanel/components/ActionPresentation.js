import React, { Component } from 'react'
import { Button } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import './actionPresentation.css';

import { connect } from 'react-redux';
import { updateSlid, setSelectedSlid, sendNavCmd } from '../../../actions'

class ActionPresentation extends Component {

    handleAdd = () => {
        this.props.dispatch(updateSlid({
            title: "new slid",
            txt: "description",
            content_id: "temp",
            id: -1
        }))
    }

    handleRemove = () => {
        if (this.props.selected_slid.id) {
            this.props.dispatch(updateSlid(this.props.selected_slid))
            this.props.dispatch(setSelectedSlid({}))
        }
    }

    handleSave = () => {
        this.props.dispatch(sendNavCmd("SAVE_CMD"))
    }

    render() {
        return (
        <div className="actions">
            <Button onClick={this.handleAdd} variant="fab" mini color="primary" aria-label="Add" className="action">
                <AddIcon />
            </Button>
            <Button onClick={this.handleRemove} variant="fab" mini color="primary" aria-label="Remove" className="action">
                <DeleteIcon />
            </Button>
            <Button onClick={this.handleSave} variant="fab" mini color="primary" aria-label="Save" className="action">
                <SaveIcon />
            </Button>
        </div>
        )
    }
}

const mapStateToProps = (state) => { 
    return {
        selected_slid: state.selectedReducer.slid,
    }
};

export default connect(mapStateToProps)(ActionPresentation)
