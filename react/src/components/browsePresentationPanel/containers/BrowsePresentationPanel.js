import React, { Component } from 'react'
import Presentation from '../../common/presentation/containers/Presentation';
import ActionPresentation from '../components/ActionPresentation';

export default class BrowsePresentationPanel extends Component {
    render() {
        return (
        <div className="vertical-scroll h-100">
            <ActionPresentation />
            <Presentation />

        </div>
        )
    }
}
