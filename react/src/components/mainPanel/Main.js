import React, { Component } from 'react'
import './main.css';
import '../../lib/bootstrap-4.1.3-dist/css/bootstrap.min.css';
import * as contentMapTmp from '../../source/contentMap.json';
import * as presTmp from '../../source/pres.json';
import BrowseContentPanel from '../browseContentPanel/containers/BrowseContentPanel';
import EditSlidPanel from '../editSlidPanel/containers/EditSlidPanel';
import BrowsePresentationPanel from '../browsePresentationPanel/containers/BrowsePresentationPanel';
import PresentationNavigation from '../editSlidPanel/containers/PresentationNavigation';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import globalReducer from '../../reducers';
import { updateContentMap, updatePresentation } from '../../actions';

import Comm from '../../services/Comm.js';

const store = createStore(globalReducer);

export default class Main extends Component {
    constructor(props) {
        super(props);

        this.comm = new Comm();

        store.dispatch(updateContentMap(contentMapTmp.default));
        store.dispatch(updatePresentation(presTmp.default));

        store.subscribe(() => {
            this.setState({
                presentation: store.getState().updateModelReducer.presentation
            })

            this.setState({
                contentMap: store.getState().updateModelReducer.content_map
            })

            switch (store.getState().commandReducer.cmdPres) {
                case 'SAVE_CMD':
                    this.comm.savPres(
                        store.getState().updateModelReducer.presentation, 
                        this.callbackErr
                    )
                    break
                case 'FIRST_CMD':
                    this.comm.begin()
                    break
                case 'PREVIOUS_CMD':
                    this.comm.backward()
                    break
                case 'PLAY_CMD':
                    this.comm.play(store.getState().updateModelReducer.presentation.id)
                    break
                case 'STOP_CMD':
                    this.comm.pause()
                    break
                case 'NEXT_CMD':
                    this.comm.forward()
                    break
                case 'LAST_CMD':
                    this.comm.end()
                    break
                default:
                    break
            }
        })

        this.comm.loadContent(this.loadContentUpdate, this.callbackErr);
        this.comm.loadPres("efa0a79a-2f20-4e97-b0b7-71f824bfe349", this.loadPresUpdate, this.callbackErr);
        this.comm.socketConnection(0);
    }

    loadContentUpdate = (data) => {
        store.dispatch(updateContentMap(data));
    }
    loadPresUpdate = (data) => {
        store.dispatch(updatePresentation(data));
    }
    callbackErr = (msg) => { 
        console.error('Network Failure ?'); 
        console.error(msg);
    }
    
    render() {
        return (
            <Provider store={store}>
                <div className="container-fluid h-100 vertical-scroll">
                    <div className="row h-100">
                        <div className="col-md-3 col-lg-3 h-100 vertical-scrool pt-3">
                            <BrowsePresentationPanel />
                        </div>
                        <div className="col-md-6 col-lg-6 h-100 vertical-scrool">
                            <PresentationNavigation />
                            <EditSlidPanel />
                        </div>
                        <div className="col-md-3 col-lg-3 h-100 pt-3 vertical-scrool">
                            <BrowseContentPanel />
                        </div>
                    </div>
                </div>
            </Provider>
        );
    }
}
    