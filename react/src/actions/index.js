export const setSelectedSlid = (slid_obj) => { 
    return {
        type: 'UPDATE_SELECTED_SLID',
        obj:slid_obj 
    };
}

export const updateContentMap = (contentMap) => {
    return {
        type: 'UPDATE_CONTENT_MAP',
        obj: contentMap
    };
}

export const updatePresentation = (pres) => {
    return {
        type: 'UPDATE_PRESENTATION',
        obj: pres
    };
}

export const updateSlid = (slid) => {
    return {
        type: 'UPDATE_PRESENTATION_SLIDS',
        obj: slid
    };
}

export const updateDraggedElt = (id) => {
    return {
        type: 'UPDATE_DRAGGED_ELT',
        obj: id
    };
}

export const addContent = (content) => {
    return {
        type: 'ADD_CONTENT',
        obj: content
    }
}

export const sendNavCmd = (cmd) => {
    return {
        type: 'COMMAND_PRESENTATION',
        obj: cmd
    }
}