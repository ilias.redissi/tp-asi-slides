const selectedReducer = (state={slid:{}}, action) => { 
    console.log(action);
    switch (action.type) {
        case 'UPDATE_SELECTED_SLID':
            return {slid: action.obj};
        case 'UPDATE_DRAGGED_ELT':
            return Object.assign({}, state, {
                slid: {
                    ...state.slid,
                    content_id: action.obj
                }
            });
        default: 
        return state;
    }
}

export default selectedReducer;