const commandReducer = (state={cmdPres: ""}, action) => { 
    switch (action.type) {
        case 'COMMAND_PRESENTATION':
            return {cmdPres: action.obj};
        default:
            return state; 
    }
}

export default commandReducer;