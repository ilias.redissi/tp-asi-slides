import Tools from '../services/Tools'

const updateModelReducer = (state={presentation:{slidArray:[]}, contentMap:{}}, action) => { 
    console.log(action);
    switch (action.type) {
        case 'UPDATE_PRESENTATION':
            return Object.assign({}, state, {
                presentation: action.obj
            });
        case 'UPDATE_PRESENTATION_SLIDS':
            if (action.obj.id === undefined) {
                return
            }

            if (action.obj.id === -1) {
                let id = Tools.generateUUID()
                action.obj.id = id
                return Object.assign({}, state, {
                    presentation: {
                        ...state.presentation,
                        slidArray: [...state.presentation.slidArray, action.obj]
                    }
                });
            }

            let index = state.presentation.slidArray.indexOf(action.obj)

            if (index > -1) {
                return Object.assign({}, state, {
                    presentation: {
                        ...state.presentation,
                        slidArray: [
                            ...state.presentation.slidArray.slice(0, index),
                            ...state.presentation.slidArray.slice(index + 1)
                        ]
                    }
                });
            }

            const indexSlid = state.presentation.slidArray.findIndex(slid => slid.id === action.obj.id)
            const slidArray = state.presentation.slidArray.map((value, index) => {
                if (index !== indexSlid) {
                    return value
                }

                return action.obj
            })

            return Object.assign({}, state, {
                presentation: {
                    ...state.presentation,
                    slidArray: slidArray
                }
            });
        case 'UPDATE_CONTENT_MAP':
            return Object.assign({}, state, {
                contentMap: action.obj
            });
        case 'ADD_CONTENT':
            let id = Tools.generateUUID()
            action.obj.id = id
            return Object.assign({}, state, {
                contentMap: {
                    ...state.contentMap,
                    [id]: action.obj
                }
            });
        default:
            return state; 
    }
}
    
export default updateModelReducer;