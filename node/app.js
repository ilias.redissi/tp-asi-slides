'use strict'

const express = require("express")
const http = require("http")
const CONFIG = require("./config.json")
process.env.CONFIG = JSON.stringify(CONFIG)

const app = express()
const server = http.createServer(app)
app.use(express.json())

const defaultRoute = require("./app/routes/default.route.js")
const presRoute = require("./app/routes/pres.route.js")
const contentRout = require("./app/routes/content.route.js")
app.use(defaultRoute)
app.use(presRoute)
app.use(contentRout)

const path = require("path")
app.use("/admin", express.static(path.join(__dirname, "public/admin")))
app.use("/watch", express.static(path.join(__dirname, "public/watch")))

const IOController = require("./app/controllers/io.controller.js");
IOController.listen(server);

server.listen(CONFIG.port)
