const socketIO = require('socket.io')

module.exports = class IOController {

    static listen(httpServer) {
        const map = new Map()
        const io = socketIO(httpServer)
        io.on('connection', (socket) => {
            socket.emit("connection")

            socket.on('_data_comm_', (msg) => {
                console.log('data', msg)
                map.set(msg.id, socket)
                
            })

            socket.on('slidEvent', (msg) => {
                console.log('slidEvent', msg)
                socket.emit('currentSlidEvent')
            })
            console.log('a user connected');
            socket.on('disconnect', function(){
                console.log('user disconnected');
            });
        })
    }

}