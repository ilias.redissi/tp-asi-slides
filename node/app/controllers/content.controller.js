var express = require("express");
const fs = require('fs')
const path = require('path')
const CONFIG = JSON.parse(process.env.CONFIG)
var ContentModel = require('./../models/content.model');



module.exports = class Contentcontroller {

    constructor(object) {
        if (!object) {
            object = {}
        }

    }



    static list(req, res){

      var list = [];
      this._readFiles('./content/', function(filename, content) {
        let data = {id:filename, content:content};
        list.push(data);
      }, function(err) {
        throw err;
      });
    }

    static _readFiles(dirname, onFileContent, onError) {
      fs.readdir(dirname, function(err, filenames) {
        if (err) {
          onError(err);
          return;
        }
        filenames.forEach(function(filename) {
          fs.readFile(dirname + filename, 'utf-8', function(err, content) {
            if (err) {
              onError(err);
              return;
            }
            onFileContent(filename, content);
          });
        });
      });
    }

    static create(req, res){
        var content = new ContentModel();
        if (!req.body) {
            console.log("No file received");
            return res.send({
                success: false
            });

        } else {
            let pres = req.body
            let presType = pres.type

            if (!presType){
                res.status(400)
                return res.end()

            }else if(presType === 'img'){
                content.type = 'img'
                content.title = pres.title
                content.file = pres.file
                content.id = pres.id
                content.fileName = req.file.originalname
                console.log(req.file.path)
                fs.readFile(req.file.path, (err, data) => {
                    if (err) {
                        console.error(err)
                        return
                    }
                    content.setData(data)
                    ContentModel.create(content, (err) => {
                        res.send({
                            success: err === undefined
                        });
                        res.end();
                    });
                })


            } else {
                content.type = pres.type
                content.title = pres.title
                content.src = pres.src
                content.id = pres.id
                ContentModel.create(content, (err) => {
                    res.send({
                        success: err === undefined
                    });

                    res.end();
                });
            }


        }
    }

    static read(req, res){
        //demandé le content
        let id = req.params.contentId
        console.log(id);
        ContentModel.read(id, (err, content) => {
            //détérminé si le content et une image
            console.log(content.type)
            if(content.type === 'img'){
                // renvoiyé donné

                fs.writeFile('/tmp/', content.getData(), 'utf8', (err) => {
                    if (err) {
                        return reject(err)
                    }
                    res.sendFile("/tmp/" + content.fileName)

                    res.end();
                })

            } else if (req.param.json === 'true'){
                // renvoiyé donné
                let data
                data.type = content.type
                data.title = content.title
                data.src = content.src
                data.id = content.id
                dataString = JSON.stringify(data)
                res.send(dataString)

                res.end();
            } else {
                // renvoiyé donné
                console.log(content.src)
                res.redirect(content.src)

                res.end();
            }

        })

    }

}
