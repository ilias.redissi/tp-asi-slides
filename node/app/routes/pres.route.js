'use strict'

const express = require("express")
const router = express.Router()
const fs = require("fs")
const path = require('path')
const CONFIG = JSON.parse(process.env.CONFIG)
const jsonExt = ".json"
const presExt= ".pres.json"

module.exports = router

router.route("/loadPres")
.get((req, res) => {
    fs.readdir(CONFIG.presentationDirectory, (err, files) => {
        if (err) {
            return res.end(err)
        }
        const presFiles = files.filter(file => path.extname(file) === jsonExt)
        const result = {}
        presFiles.forEach(file => {
            let filepath = path.resolve(CONFIG.presentationDirectory, file)
            let pres = JSON.parse(fs.readFileSync(filepath, "utf8"))
            result[pres.id] = pres
        })
        res.json(result)
        res.end()   
    })
})

router.route("/savePres")
.post((req, res) => {
    if (!req.body) {
        res.status(400)
        return res.end()
    }
    let pres = req.body
    let presId = pres.id
    if (!presId) {
        res.status(400)
        return res.end()
    }
    let presPath = path.resolve(CONFIG.presentationDirectory, presId + presExt)
    fs.writeFile(presPath, JSON.stringify(pres), (err) => {
        if (err) {
            return res.end(err)
        }
        res.status(201)
        res.end()
    })
    
})