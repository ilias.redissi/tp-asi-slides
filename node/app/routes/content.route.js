// content.route.js
var multer = require("multer");
var express = require("express");

var contentController = require('./../controllers/content.controller');
controller = Object.create(contentController);
var router = express.Router();
module.exports = router;

var multerMiddleware = multer({ "dest": "/tmp/" });

router.get("/contents", function(req, res) {
    contentController.list(req, res);
    res.end();
});

router.post("/contents",multerMiddleware.single("file"), function(req, res) {
    console.log("req routed");
    controller.create(req, res);
});

router.get("/contents/:contentId", function(req, res) {
    console.log("req routed");
    contentController.read(req, res);
});
