'use strict'
const fs = require('fs')
const path = require('path')
const CONFIG = JSON.parse(process.env.CONFIG)

const metaExt= ".meta.json"

module.exports = class ContentModel {

    constructor(object) {
        if (!object) {
            object = {}
        }
        this.id = (object['id'] === undefined) ? null : object.id
        this.fileName = (object['fileName'] === undefined) ? null : object.fileName
        this.src = (object.src === undefined) ? null : object.src
        this.title = (object.title === undefined) ? null : object.title
        this.type = (object.type === undefined) ? null : object.type
        let _data = null
        this.setData = (data) => { _data = data }
        this.getData = () => { return _data }
    }

    static create(content, callback) {
        if (!content) {
            callback(Error("content is null"))
            return
        }

        if (!(content instanceof ContentModel)) {
            callback(Error("content is not a ContentModel"))
            return
        }

        if (!content.id) {
            callback(Error('content\'s id is null'))
            return
        }

        this._writeData(content)
            .then(this._writeMeta(content))
            .then(() => {
                callback()
            })
            .catch(reason => {
                console.error(reason)
                callback(reason)
            })
    }

    static _writeData(content) {
        return new Promise((resolve, reject) => {
            if (content.type === 'img') {
                let dataPath = path.resolve(CONFIG.contentDirectory, content.fileName)
                console.log(content.getData())
                console.log(dataPath)
                fs.writeFile(dataPath, content.getData(), (err) => {
                    if (err) {
                        return reject(err)
                    }
                    resolve()
                })
                return
            }
            resolve()
        })
    }

    static _writeMeta(content) {
        return new Promise((resolve, reject) => {
            let metaPath = path.resolve(CONFIG.contentDirectory, content.id + metaExt)

            fs.writeFile(metaPath, JSON.stringify(content), (err) => {
                if (err) {
                    return reject(err)
                }
                resolve()
            })
        })

    }

    static read(id, callback) {
        let metaPath = path.resolve(CONFIG.contentDirectory, id + metaExt)
        fs.readFile(metaPath, 'utf8', (err, data) => {
            if (err) {
                callback(err)
                return
            }
            let content = new ContentModel(JSON.parse(data))
            callback(null, content)
        })
    }

    static update(content, callback) {
        if (!content) {
            callback(Error("content is null"))
            return
        }

        if (!(content instanceof ContentModel)) {
            callback(Error("content is not a ContentModel"))
            return
        }

        if (!content.id) {
            callback(Error('content\'s id is null'))
            return
        }

        let metaPath = path.resolve(CONFIG.contentDirectory, content.id + metaExt)
        if (!fs.existsSync(metaPath)) {
            callback(Error('content is not created'))
            return
        }

        this._writeData(content)
            .then(this._writeMeta(content))
            .then(() => {
                callback()
            })
            .catch(reason => {
                console.error(reason)
                callback(reason)
            })
    }

    static delete(id, callback) {
        if (!id) {
            callback(Error("id is null"))
            return
        }

        let metaPath = path.resolve(CONFIG.contentDirectory, id + metaExt)
        if (!fs.existsSync(metaPath)) {
            callback(Error('content is not created'))
            return
        }

        this._deleteData(id)
            .then(this._deleteMeta(id))
            .then(() => {
                callback()
            })
            .catch(reason => {
                console.error(reason)
                callback(reason)
            })
    }

    static _deleteData(id) {
        return new Promise((resolve, reject) => {
            this.read(id, (err, content) => {
                if (err) {
                    reject(err)
                    return
                }

                let dataPath = path.resolve(CONFIG.contentDirectory, content.fileName)
                fs.unlink(dataPath, (err) => {
                    if (err) {
                        reject(err)
                        return
                    }
                    resolve()
                })
            })
        })
    }

    static _deleteMeta(id) {
        return new Promise((resolve, reject) => {
            let metaPath = path.resolve(CONFIG.contentDirectory, id + metaExt)
            fs.unlink(metaPath, (err) => {
                if (err) {
                    reject(err)
                    return
                }
                resolve()
            })
        })
    }
}
